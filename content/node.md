+++
title = "Installing NodeJS on Linux (LTS)"
date = 2021-04-29T21:40:57+02:00
+++
I am doing this quick guide because I think node isn't very easy to install. There are plenty of different installation means and it can get very quickly confusing because some are distro-specific, some are outdated, etc.<!-- more -->

In this tutorial I'm gonna show you **my** way to install Node, I don't say it's the best, but I think it's pretty easy, non-specific and global (for all users).

## Step 1: Download Node
Before downloading, we need to get the url. Simply go on [Node's website](http://nodejs.org/) and copy the download url of the LTS release.

Then we to go in the directory to install node and download the archive using `wget`.

```bash
cd /usr/local
wget <the url you copied earlier>
```

## Step 2: Extract Node
Then we need to unpack Node:

```bash
# Extract all tar files (there is only one in this case)
tar xvf *.tar.* 
# Remove the tar file (which is now extracted)
rm *.tar.*
# Rename the directory into something easier to understand
mv node*/ node/
```

## Step 3: Link the binaries
Now we need to link the binaries so you can use the commands "node" and "npm" globally (by any user):

```bash
ln -s /usr/local/node/bin/* /usr/bin/
```

## Step 4: Testing
You can now test if Node and Npm have been installed with the correct versions using the following commands:

```bash
node -v
npm -v
```

That's it, now you have Node, Npm and Npx installed on your device!
