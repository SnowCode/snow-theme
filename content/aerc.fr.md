+++
title = "Gérer ses e-mails avec aerc."
date = 2021-05-02T21:06:00+02:00
+++
`aerc` qui veut dire... Bah, j'ai pas trouvé ce que signifie "aerc" en fait. Mais c'est un client mail en terminal qui est super bien.<!-- more -->

J'utilisait `mutt` avant, mais `aerc`, contrairement à `mutt` supporte les mails en HTML et, truc tout con mais incroyablement emmerdant : supporte les long liens par défault.

J'avoue avoir été plus tôt flemmard avec `mutt`, donc j'ai pas pris le temps de voir toutes les options de configuration, donc les problèmes cités ci-dessus n'en sont probablement pas. Mais fait est que je trouve `aerc` beaucoup plus simple.

Voyons comment installer et configurer ce client.

## Pour les utilisateurs Gmail
J'utilise toujours Gmail (malheureusement), et Google bloque les clients "non-officiels" par défault, il faut donc changer quelques paramètres.

Premièrement il faut se connecter sur [cette page](https://myaccount.google.com/) et aller dans l'onglet "Sécurité".

Ensuite il faut activer l'authentification en 2 étapes, puis aller dans "Mots de passe des applications".

Enfin, dans "Sélectionnez une application" il faut choisir "Autre" que l'on va nommer ici "aerc". Enfin cliquez sur "Générez" et copiez le mot de passe (il ne sera plus accessible plus tard)

## Installer aerc
Maintenant on va installer aerc. Sur certaines distributions (Arch, Alpine et macOS), il est disponible dans le gestionaire de paquets par défault. Je suis actuellement sur Debian donc ce n'est pas mon cas.

Il faut ensuite installer les dépendences, ici c'est go et scdoc.

Voici la commande pour installer go et scdoc sous Debian :

```bash
sudo apt install golang scdoc
# Attention je ne sais pas si le paquet go est à jour, il vaut mieux l'installer à partir du site officiel
```

On va ensuite télécharger le code source :

```bash
git clone https://git.sr.ht/~sircmpwn/aerc
cd aerc/
```

On va enfin compiler et installer aerc...

```bash
make
make install
```

## Configuration
Pour configurer, rien de plus simple, il suffit de lançer `aerc`, de donner les informations requises sur les étapes 1 et 2, pour tout le reste, il suffit d'utiliser les paramètres par défault.

## Utilisation
Pour savoir comment utiliser `aerc` il y a la commande `man aerc-tutorial` qui va vous donner toutes les raccourcis de clavier disponibles. Vous pouvez aussi les modifier dans `~/.config/aerc/binds.conf`.
