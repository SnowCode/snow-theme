+++
title = "Comment utiliser IRC"
date = 2021-02-25T17:56:44+01:00
+++
J'avoue, je n'utilise pas des masse IRC, mais c'est plus tot à cause d'un manque de contenu qui m'interesse et non pour des raisons techniques.<!-- more -->

## Résumé
Avant de commencer, pour ne pas vous perdre je vais décrire comment IRC fonctionne.

Des clients (vous avec un logiciel approprié) se connecte à des serveurs. Le protocol est très simple, vous devez lançer des commandes (et des messages) que le serveur interprète. Ensuite le client (donc le logiciel que vous utilisez) vous montre le résultat:

```bash
Client  | /join #freenode
Serveur | <Fait rejoindre l'utilisateur dans le channel #freenode>
Client  | Bonjour \o/
Serveur | <envoie le message aux autres utilisateurs>
```

Ce qui signifie que, quand vous n'êtes pas connecté, vous ne recevez aucun message ! C'est pour cela qu'il y a ce que l'on appel des "bouncer". Un bouncer est un serveur qui va rester connecté 24/7 pour que vous soyez au courrant de tous les messages.

## Bouncer et client en même temps
La majorité du temps, les bouncer et les clients sont 2 logiciels séparé, mais j'ai trouvé un moyen de faire les deux en même temps et de manière plus synchronisée.

Ce moyen c'est d'utiliser tmux, weechat (ou autre client en terminal) et SSH.

Tout d'abord on va installer tmux et weechat sur le serveur.

```bash
ssh <nom d'utilisateur>@<ip>
sudo apt install tmux weechat
```

Ensuite on va créer une nouvelle session tmux avec weechat dedans

```bash
tmux new -s weechat weechat
```

Pour fermer cette session, n'utilisez JAMAIS "/quit", utilisez CTRL+B puis D. 

Ensuite quittez la session SSH:

```bash
logout
```

Puis créez un nouvel alias qui va vous permettre d'accéder rapidement à IRC:

```bash
echo "alias irc='ssh <username>@<ip address> -t tmux attach -t weechat'" >> ~/.bashrc
```

Il y a plusieurs avantages à utiliser ceci à la place de bouncers classiques. 

* Tout est synchronisé et clair, les messages apparaissent de la même manière sur tous les appareils. 
* Lightweight
* Simple, pas de configuration spéciale pour le bouncer
* Pas besoin de plusieurs clients. N'importe quel appareil pouvant utiliser SSH pour se connecter à votre serveur peut être utilisé pour se connecter à IRC, même un téléphone (utilisez Termux :P)

## Configuration Weechat
Enfin on va configurer le client (se connecter, ajouter des filtres, etc)

Tout d'abord on va aller sur irc:

```bash
irc
```

Ensuite on va ajouter un nouveau serveur

```bash
/server add <name> <url>
/server add freenode chat.freenode.org
/connect freenode
```

On peut maintenant s'authentifier

```bash
/nick <nom d'utilisateur>
/msg NickServ REGISTER <mot de passe> <email>
/msg NickServ IDENTIFY <mot de passe>
```

On peut également rejoindre des salons

```bash
/join #freenode
/join ##linux
```

Enfin, j'ai aussi ajouté une commande /filter qui permet d'éliminé tous les "truc est entré, machin est sorti, etc"

```bash
/filter add joinquit * irc_join,irc_part,irc_quit *
```

Pour naviguer entre les channels avec weechat il faut utiliser ALT+UP et ALT+DOWN. 

