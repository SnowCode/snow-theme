+++
title = "Installer Searx pour améliorer sa vie privée"
date = 2021-02-02T12:00:45+01:00
+++
Google n'est pas bon pour protéger la vie privée de ses utilisateurs. Mais ça on le sait déjà. <!-- more -->

Le moteur de recherche le plus connu pour palier ce besoin est DuckDuckGo. Le problème est que rien ne prouve que DDG protège mieux nos données que Google. 

C'est dans cette optique de vie privée que searX a été créé. searX est un meta-engine, ce qui signifie qu'il anonymise les données avant de les envoyer à d'autres sites, et inversément. 

Donc quand on fait une recherche sur searX : 

1. SearX va anonymiser les requêtes
2. SearX va envoyer les requêtes anonymisées à différents moteurs de recherches comme Google, DuckDuckGo, etc. (Il y a 70 moteurs de recherches disponibles :p)
3. SearX va retourner les résutats de recherches seulement, sans le tracking et les pubs.

SearX est donc un excellent outil pour protéger sa vie privée. Dans ce billet jen vais expliquer comment l'installer locallement sur son ordinateur. Un serveur n'est pas nécessaire pour searx. 

Le seul avantage de l'installer sur un serveur publique étant de pouvoir l'utiliser sur mobile ou de le partager avec quelqu'un qui ne l'a pas installé.

## Installer les dépendences
Tout d'abord on va installer git et python3

```bash
sudo su
apt -y install git python3 python3-pip
```

Ensuite, on va télécharger le code source de SearX dans le dossier `/opt`

```bash
cd /var/www/
git clone https://github.com/searx/searx
cd searx
```

Enfin, on va installer toutes les dépendences

```bash
pip3 install setuptools wheel pyyaml
pip3 install -e .
```

On peut maintenant tester si il fonctionne en faisant:

```bash
python3 /var/www/searx/searx/webapp.py
```

## Création d'un nouveau service
Ouvrons un nouveau fichier `searx.service`

```bash
vim /etc/systemd/system/searx.service
```

Voici la configuration du service

```systemd
[Unit]
Description=Searx
After=network.target

[Service]
Restart=always
RestartSec=1
ExecStart=python3 /var/www/searx/searx/webapp.py

[Install]
WantedBy=multi-user.target
```

Enfin on peut lançer notre nouvelle configuration

```bash
systemctl daemon-reload
systemctl restart searx
systemctl status searx
```

## Configuration DNS et HTTPS
On va créer 2 nouvelles entrées DNS

| Domain | TTL | Type | Target |
| --- | --- | --- | --- |
| `searx.domain.tld` | `0` | `A` | `<ipv4>` |
| `searx.domain.tld` | `0` | `AAAA` | `<ipv6>` |

Ensuite on va cretifier le nom de domaine avec letsencrypt

```bash
sudo apt install -y letsencrypt
sudo systemctl stop nginx
certbot certonly --standalone -d searx.domain.tld
sudo systemctl start nginx
```

## Configuration nginx
On va créer un nouveau fichier de configuration 

```bash
vim /etc/nginx/sites-available/searx.conf
```

Et coller la config suivante : 

```nginx
server {
	listen 80;
	listen [::]:80;

	server_name searx.domain.tld;

	return 301 https://searx.domain.tld$request_uri;
}

server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	location / {
            proxy_pass http://127.0.0.1:8888;
	    proxy_set_header Host $host;
            proxy_set_header Connection       $http_connection;
	    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Scheme $scheme;
            proxy_buffering off;
	}

	include /etc/nginx/snippets/letsencrypt.conf;

	server_name searx.domain.tld;

	ssl_certificate /etc/letsencrypt/live/searx.domain.tld/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/searx.domain.tld/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/searx.domain.tld/fullchain.pem;

	access_log /var/log/nginx/searx.domain.tld.access.log;
	error_log /var/log/nginx/searx.domain.tld.error.log;
}
```

Enfin on peux appliquer la configuration

```bash
ln -s /etc/nginx/sites-available/searx.conf /etc/nginx/sites-enabled/
nginx -t
systemctl restart nginx
systemctl restart searx
```

Normalement, Searx devrait être disponible à `https://searx.domain.tld`.


## Le mettre par défault sur Firefox
Firefox a une manière, pour le moins, spéciale... De définir un moteur de recherche personalisé. 

1. Aller dans `about:preferences#search` et sélectionner `Add search bar in toolbar`

2. Aller sur `http://localhost:8888` et cliquer sur l'icone verte de la nouvelle barre, puis sur `Add "searx"` 

3. Cliquez droit sur l'icone de searx dans cette même barre et cliquez sur `Set as default search engine` 

4. On peut maintenant retirer cette vilaine barre que personne n'utilise et qui prends toute la place en retournant dans les paramètres.

Et voilà, searx est installé et fonctionnel. On peut maintenant le configurer pour fonctionner avec d'autres moteurs de recherches, d'autres aparences, etc. 

