+++
title = "How to improve Discord with Powercord"
date = 2022-04-08
[taxonomies]
tags = ["tutorial", "discord"]
[extra]
image = "/powercord.png"
+++
How to improve Discord's look as well as features. How to implement essential Discord features by yourself.<!-- more -->

Powercord is a Discord client mod (which modifies your Discord application), it's pretty lightweight, and has a lot of themes and plugins available.

## Installing dependencies
First, let's start with the ingredients:

* Git
* Node and npm (v10 minimum) 
* [Discord canary (developpement version of Discord)](https://discord.com/api/download/canary?platform=linux)


## Installing Discord canary
The base of everything is the Discord canary client, this client is an always up to date client with cutting edge features.

First download the tar.gz of it [here](https://discord.com/api/download/canary?platform=linux).

Let's go! 

```bash
sudo rm /usr/bin/discord
sudo cp ~/Downloads/discord-canary*.tar.gz /opt
cd /opt
sudo tar xvf discord-canary*.tar.gz
sudo ln -s /opt/DiscordCanary/DiscordCanary /usr/bin/discord
sudo rm discord-canary*.tar.gz
```

Now you can run Discord as usual and it should update, ask for your password and that should be DiscordCanary.

## Installing Powercord
Let's install powercord now...

```bash
cd ~
git clone https://github.com/powercord-org/powercord
cd powercord/
npm install
sudo npm run plug
```

## Some plugins to install
My three favorite plugins are:

* `hide-blocked-messages` which does just that, hiding blocked messages for good.
* `theme-toggler` if you have multiple themes that can be handy
* `better-settings` which let you have a search bar in your Discord settings.

The following code will install all those three plugins for you. You'll have to restart Discord however.

```bash
cd ~/powercord/src/Powercord/plugins
git clone https://github.com/beerpiss/hide-blocked-messages
git clone https://github.com/redstonekasi/theme-toggler
git clone https://github.com/mr-miner1/better-settings
```

If you want to find other plugins to install you can checkout the list [here](https://github.com/topics/powercord-plugin)

## My theme
My favorite theme is MinimalCord but I change from time to time. Here's how to install it:

```bash
cd ~/powercord/src/Powercord/themes/
git clone https://github.com/DiscordStyles/MinimalCord
```

If you want to check out other themes, you can go on [this link](https://github.com/topics/powercord-theme)
