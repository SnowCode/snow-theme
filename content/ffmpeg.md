+++
title = "Tips and tricks with ffmpeg"
date = 2021-08-26T21:29:22+02:00
+++
This is a quick cheatsheet for me and for anyone that would be interested by ffmpeg. I use ffmpeg for many things but especially: screen recording and to be able to upload larger files on Discord without Nitro.<!-- more -->

## Recording screen and audio
* Recording screen without audio

```bash
ffmpeg -f x11grab -i :0.0 -preset ultrafast screenrecording.mkv
```

* Recording screen with audio

```bash
ffmpeg -f x11grab -i :0.0 -f pulse -ac 2 -i default -preset ultrafast screenrecording.mkv
```

* Recording audio without screen

```bash
ffmpeg -f pulse -ac 2 -i default audiorecording.mp3
```

## Shrink file size
To be able to upload files on Discord (or just have easier time to edit the video if your computer is low end) you can shrink the file isze by a lot this way:

* Shrink file size by re-encoding. This doesn't really reduce quality but has a huge impact on filesize (sometimes even x10 times smaller).

```bash
ffmpeg -i input.mkv -vcodec libx265 -crf 28 output.mp4
```

* Make the file previewable (I've had the bad surprise the files converted by ffmpeg cannot be previewed in WhatsApp and Discord). This may increase the file size a bit. 

```bash
ffmpeg -i input.mp4 -movflags faststart output.mp4
```

EDIT: the above command might be completely wrong, try this one instead:

```bash
ffmpeg -i input.mp4 -pix_fmt yuv420p output.mp4
```
