+++
title = "Installing Etherpad"
date = 2021-04-29T17:19:35+02:00
+++
In this post I'm gonna explain how to install and configure Etherpad on your server (for production).

We're going to: install NodeJS, install Etherpad, setup subdomain, setup HTTPS, setup nginx, create a service file for Etherpad, create a database for Etherpad, change the default Etherpad theme and install new plugins. All of this in less than half an hour...<!-- more -->

> **Note**, you need to have a server running nginx and a domain name to follow this guide. If you want to secure your server, you can read [this post](https://snowcode.ovh/posts/secure/).

## Step 1: Installing dependencies
You need to install node.js (minimum v10.17). To install the latest stable version though, you can do this:

First, go on the nodejs website [here](https://nodejs.org/en/) and **copy** the link to the LTS version.

Now in your terminal you can do the following:

```bash
cd /usr/local/                          # Going into the directory to download
wget <paste your url here>              # Downloading
tar xvf node*.tar.*                     # Extract the content
mv node-*/ node/                        # Rename the directory
ln -s /usr/local/node/bin/* /usr/bin    # Link all binaries into the /usr/bin directory
rm *.tar.*                              # Remove the downloaded archive for cleaning
```

## Step 2: Creating an "etherpad" user
We now need to create a specific user for etherpad

```bash
useradd -s /usr/bin/bash -m etherpad
passwd etherpad
```

## Step 3: Downloading Etherpad
Now that we have all the ingredients to make Etherpad, we can now download the source code of Etherpad.

First, we will download the source code:

```bash
cd /var/www        # Or where ever you want to store Etherpad's files
git clone --branch master https://github.com/ether/etherpad-lite.git # Download Etherpad
```

Then you need to asign the downloaded source code to the user etherpad for security: (then login as etherpad and enter the directory)

```bash
chown etherpad:etherpad -R etherpad-lite/   # Assign yourself the right permissions
su - etherpad                               # Login as etherpad
cd /var/www/etherpad-lite/                  # Go back into the directory
```

You can now test if everything worked properly by running:

```bash
./src/bin/run.sh    # Run the start script
exit                # Exit the etherpad user
```

## Step 4: Create a new service file
Open a new service file:

```bash
nano /etc/systemd/system/etherpad.service
```

And paste the following content:

```bash
[Unit]
Description=Etherpad
After=network.target network-online.target

[Service]
Type=simple
User=etherpad
Group=etherpad
Restart=always
RestartSec=1
ExecStart=/var/www/etherpad-lite/src/bin/run.sh

[Install]
WantedBy=multi-user.target
```

You can now start etherpad:

```bash
sudo systemctl start etherpad
```

## Step 5: Create an HTTPS subdomain name
First, go on your DNS Zone and create 2 new entries for your subdomain, for instance:

| Domain | Type | IP |
| --- | --- | --- |
| `pad.example.com` | `A` | `12.45.67.44` |
| `pad.example.com` | `AAAA` | `2a02:a03f:a1de:4600:467a:291d:a613:c637` |

Then certify the domain name:

```bash
sudo systemctl stop nginx
certbot certonly --standalone -d pad.example.com
```

## Step 6: Create a reverse proxy
Open a new nginx configuration file

```bash
nano /etc/nginx/sites-available/etherpad.conf
```

And paste the following content: (Don't forget to replace `pad.example.com` by your real domain.

```nginx
server {
    listen 80;
    listen [::]:80;

    server_name pad.example.com;

    return 301 https://pad.example.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    location / {
        proxy_pass http://127.0.0.1:9001;

        proxy_set_header Host $host;
        proxy_set_header Connection       $http_connection;
        proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme         $scheme;
        proxy_buffering                   off;
    }

    include /etc/nginx/snippets/letsencrypt.conf;

    server_name pad.example.com;

    ssl_certificate /etc/letsencrypt/live/pad.example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/pad.example.com/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/pad.example.com/fullchain.pem;

    access_log /var/log/nginx/pad.example.com.access.log;
    error_log /var/log/nginx/pad.example.com.error.log;
}
```

Then we can enable our configuration and restart nginx

```bash
ln -s /etc/nginx/sites-available/etherpad.conf /etc/nginx/sites-enabled/
systemctl restart nginx
```

Etherpad is now available on your domain.

## Step 7: Change the theme (optional)
Go into the settings file.

```bash
nano /var/www/etherpad-lite/settings.json
```

Then search for "super-light" and edit the values for: the background, the editor and the toolbar.

Then you can restart etherpad...

```bash
systemctl restart etherpad
```

## Step 8: Add plugins (optional, but recommended)
First let's get into the folder as "etherpad" user.

```bash
su - etherpad
cd /var/www/etherpad-lite/
```

Then run `npm install` to install your plugins:

```bash
npm install ep_headings2 ep_font_color ep_comments_page ep_align ep_embedded_hyperlinks2 ep_markdown
```

Restart etherpad to apply the changes

```bash
exit
systemctl restart etherpad
```

## Step 9: Secure the installation
We can now create a mysql database to replace the already existing database.

First let's create the database:

```bash
mysql
CREATE USER 'etherpaduser'@'localhost' IDENTIFIED BY '<your password>';
CREATE DATABASE etherpad_lite_db;
GRANT ALL PRIVILEGES ON etherpad_lite_db.* TO 'etherpaduser'@'localhost';
FLUSH PRIVILEGES;
EXIT;
```

Now let's go into the `settings.json` file

```bash
nano /var/www/etherpad-lite/settings.json
```

Now let's search in the document for "mysql" and comment the "dirtyDB", then uncomment and edit "mysql".

Finally you can restart etherpad

```bash
systemctl restart etherpad
```

## Conclusion
You now have a working setup of Etherpad on your server, including:

* NodeJS LTS installed globally
* A subdomain and a reverse proxy
* HTTPS ceritficate for that domain
* Tool bar extension
* Production MySQL database
* Service file to manage Etherpad properly
* A custom theme

> **Note**: I don't use Etherpad anymore. Mostly because It doesn't support Markdown properly and is very bloated. I now use Git, Markdown and Gitea instead.
