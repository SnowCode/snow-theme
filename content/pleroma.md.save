+++
title = "How to setup a Pleroma instance."
date = 2021-05-01T12:32:09+02:00
+++
Pleroma is a simple and lightweight way to install your own ActivityPub / Mastodon instance on your server.<!-- more -->

## Step 0: Requirements
* A secure Debian server
* A certified sub-domain (like https://social.domain.tld for instance)

## Step 1: Install dependencies
First let's install dependencies:

```bash
apt install curl unzip libncurses5 postgresql postgresql-contrib nginx certbot libmagic-dev
apt install imagemagick ffmpeg libimage-exiftool-perl
```

## Step 2: Create a Pleroma user
Simple run:

```bash
adduser --system --shell /bin/false --home /opt/pleroma pleroma
```

## Step 3: Download Pleroma
We can now download the Pleroma binaries and move them:

```bash
su pleroma -s $SHELL
cd

curl 'https://git.pleroma.social/api/v4/projects/2/jobs/artifacts/stable/download?job=amd64' -o ~/pleroma.zip
unzip pleroma.zip

mv release/* .
rm -r release/
rm pleroma.zip
```

## Step 4: Create directories
Now let's create the different directories pleroma require and set the ownership of the folder to "pleroma".

```bash
exit # Exit the pleroma user
mkdir -p /var/lib/pleroma/uploads
mkdir -p /var/lib/pleroma/static
mkdir -p /etc/pleroma

chown -R pleroma /var/lib/pleroma
chown -R pleroma /etc/pleroma
```

## Step 5: Run the scripts
You can now run the built-in scripts to setup the config, setup the database and test it.

```bash
su pleroma -s $SHELL # Login as pleroma
./bin/pleroma_ctl instance gen --output /etc/pleroma/config.exs --output-psql /tmp/setup_db.psql # Auto-Configure
psql -f /tmp/setup_db.psql # Setup the database
./bin/pleroma_ctl migrate # Migrate the database
```

Now let's test everything worked properly:

```bash
./bin/pleroma daemon
sleep 20 && curl http://localhost:4000/api/v1/instance
./bin/pleroma stop
exit # Logout the pleroma user
```

## Step 6: Configure nginx
Pleroma has a default nginx config file you can use as template:

```bash
cp /opt/pleroma/installation/pleroma.nginx /etc/nginx/sites-available/pleroma.conf
ln -s /etc/nginx/sites-available/pleroma.conf /etc/nginx/sites-enabled/pleroma.conf
nano /etc/nginx/sites-available/pleroma.conf
```

In that file simply replace all occurences of `domain.tld` by your actual domain, example: `social.example.io`.

Once that's done, simply save and exit the file. Then type the following command to restart nginx:

```bash
systemctl restart nginx
```

## Step 7: Create the service file for Pleroma
Now to manage Pleroma, we can use the default service file (systemd), no edit required.

```bash
cp /opt/pleroma/installation/pleroma.service /etc/systemd/system/pleroma.service
systemctl start pleroma
systemctl enable pleroma
```

## Step 8: Create an admin user
Let's create an admin user now.

```bash
./bin/pleroma_ctl user new <username> <username>@<your domain name> --admin
```

Then simply go on the url that's provided by that command to change your password.

## Step 9: Use Pleroma
You now have a working instance of Pleroma. To use it simply connect using your user.

You can change your profile settings in the settings gear on the top right of the page. 

Then you need to follow other instances of other servers, if you don't, there will be no content. The "Known Network" is based on all your follows. Note you also won't have the old content of accounts, only the new ones.

This is a quick guide to understand the different timelines in Pleroma (or Mastodon in general)

* Home Timeline contains your followings, your own messages, and private messages
* Bookmarks contains all the posts you bookmarked (obviously)
* Direct Messages contains only private messages
* Public Timeline contains only public messages
* Known Network (or federated Timeline) contains all the interactions that are made with your follows

nn


