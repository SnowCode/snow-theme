+++
title = "How to make a semi-automatic crop farm"
date = 2021-07-03
[taxonomies]
tags = ["tutorial", "minecraft"]
[extra]
image = "/crop-farm/2021-07-03_17.00.58.png"
+++

Just another starter Minecraft tutorial. This is a ttorial to build a semi-automatic crop farm without redstone.<!-- more -->

## Step 1: Gather the building blocks

* 36 cobblestone walls
* 9 cobblestone slabs
* 1 lever
* 2 trapdoors
* 1 cobblestone stair
* 3 ladders
* 4 signs
* 2 buckets of water (for an infinite water source)

## Step 2: Building it

Dig a 11 by 11 square
![](/crop-farm/2021-07-03_16.53.00.png)
Dig a 2 block deep cross in the middle
![](/crop-farm/2021-07-03_16.53.25.png)
Add 4 signs on the top end of this "cross" and fill with water as shown
![](/crop-farm/2021-07-03_16.55.05.png)
Continue the water by digging a hole and add 3 ladders as shown
![](/crop-farm/2021-07-03_16.55.18.png)
Add 4 blocks on the corners and walls over the water
![](/crop-farm/2021-07-03_16.56.06.png)
Fill the cross with dirt
![](/crop-farm/2021-07-03_16.56.22.png)
Build the following structure using stairs, slabs, a lever and a waterlogged trapdoor.
![](/crop-farm/2021-07-03_16.57.25.png)
Place a slab over the water
![](/crop-farm/2021-07-03_16.57.31.png)
Place a trapdoor on the ladders
![](/crop-farm/2021-07-03_16.59.16.png)
Plant your crops
![](/crop-farm/2021-07-03_16.59.05.png)
Harvest them
![](/crop-farm/2021-07-03_17.00.58.png)
Collect them
![](/crop-farm/2021-07-03_17.01.06.png)
