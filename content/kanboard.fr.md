+++
title = "Installation de Kanboard sur Debian"
date = 2021-02-04T19:48:23+01:00
+++
Dans un article précédent j'ai parlé de `taskcafé`. C'est intérressant, mais j'avoue préférer `kanboard`, non seulement il est beaucoup plus simple à installer,
mais il est aussi beaucoup plus rapide que `taskcafé`. <!-- more -->

Donc, commençons par configurer le DNS et HTTPS.

## Configuration du DNS et HTTPS
Nous allons, comme toujours, ajouter deux nouvelles entrées dans le DNS.

| Domain | TTL | Type | Target |
| --- | --- | --- | --- |
| `board.domain.tld` | `0` | `A` | `<ipv4>` |
| `board.domain.tld` | `0` | `AAAA` | `<ipv6>` |

Maintenant nous pouvons certifier le nom de domaine avec `certbot`

```bash
apt install letsencrypt

systemctl stop nginx
certbot certonly --standalone -d board.domain.tld
systemctl start nginx
```

## Installation des dépendences PHP
Voici la commande qui devrait installer toutes les dépendences des modules php. 

```bash
apt-get install -y php7.4-mbstring php7.4-sqlite3 \
php7.4-json php7.4-gd php7.4-xml php7.4-curl php7.4-zip
```

## Téléchargement du code source
Nous pouvons maintenant prendre le code source de `kanboard`

```bash
cd /var/www/
git clone https://github.com/kanboard/kanboard
```

On va ensuite configurer les permissions du dossier `data`

```bash
chown -R www-data:www-data kanboard/data/
```

Cette toute la configuration qu'il y avait à faire du côté de `kanboard` :smile:

## Configuration nginx
Maintenant il faut dire à `nginx` que le dossier `/var/www/kanboard/` doit être connecté avec le nom de domaine `board.domain.tld`.

Donc ouvrons un nouveau fichier de configuration : 

```bash
nano /etc/nginx/sites-available/kanboard.conf
```

Et ensuite coller et personaliser la configuraiton suivante : 

```nginx
server {
	listen 80;
	listen [::]:80;

	server_name board.domain.tld;

	return 301 https://board.domain.tld$request_uri;
}

server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	root /var/www/kanboard;

	include /etc/nginx/snippets/letsencrypt.conf;

	index index.php index.html index.htm;

	server_name board.domain.tld;

	ssl_certificate /etc/letsencrypt/live/board.domain.tld/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/board.domain.tld/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/board.domain.tld/fullchain.pem;

    location ~ \.php$ {
        try_files $uri $uri/ /doku.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param REDIRECT_STATUS 200;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
    }

	access_log /var/log/nginx/board.domain.tld.access.log;
	error_log /var/log/nginx/board.domain.tld.error.log;
}
```

On va ensuite activer notre nouvelle configuration et relançer nginx pour appliquer les changements.

```bash
ln -s /etc/nginx/sites-available/kanboard.conf /etc/nginx/sites-enabled/
nginx -t
systemctl restart nginx
```

Le site devrait maintenant être accessible sur le nouveau nom de domaine. Il faut ensuite entrer les logins :

| Champ | Valeur  |
| ----- | ------- | 
| user  | `admin` |
| pass  | `admin` |

Pour quelques raisons de sécurités évidentes, on va changer le mot de passe en allant à cette page : `https://board.domain.tld/?controller=UserCredentialController&action=changePassword`

Et voilà ! C'est tout ce qu'il y a à faire !

