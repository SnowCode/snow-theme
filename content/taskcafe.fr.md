+++
title = "Installer taskcafe"
date = 2021-02-03T10:18:54+01:00
+++
Taskcafe es une webapp kanban. J'ai vu un screenshot sur Discord et j'ai ressenti un besoin irrésistible de l'installer. Donc voici comment l'installer de A à Z.<!-- more -->

## Installation des dépendences
Les dépendences de taskcafe sont pour le moins... spéciales... J'ai installé nvm pour installer node et npm pour installer yarn pour utiliser go pour installer taskcafe.

Donc commençons, tout d'abord on va installer `go`

```bash
wget https://golang.org/dl/go1.15.7.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.15.7.linux-amd64.tar.gz
echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.bashrc
source ~/.bashrc
rm go1.15.7.linux-amd64.tar.gz
```

Maintenant on va installer `node.js` et `npm`

```bash
wget https://nodejs.org/dist/v14.15.4/node-v14.15.4-linux-x64.tar.xz
tar xvf node*.tar.xz
mv node*/ /usr/local/node/
echo "export PATH=$PATH:/usr/local/node/bin/" >> ~/.bashrc
source ~/.bashrc
rm node*.tar.xz
```

Enfin, on peux installer `yarn`

```bash
npm install -g yarn
```

## Installer à partir du code source
Tout d'abord il faut télécharger le code source.

```bash
git clone https://github.com/JordanKnott/taskcafe
cd taskcafe
```

Ensuite on peux installer le tout avec `go` (qui va lui même utiliser `yarn`)

```bash
go run cmd/mage/main.go install
go run cmd/mage/main.go build
```

Notez que la deuxième commande est très consomatrice de RAM et qu'elle peux durer un certain moment.

## Mise en place de la base de donnée
Tout d'abord il faut installer `postgresql`

```bash
sudo apt install -y postgresql
```

Maintenant on peut créer notre base de donnée et l'utilisateur

```bash
sudo -u postgres psql
CREATE DATABASE taskcafe;
CREATE USER taskcafe WITH ENCRYPTED PASSWORD '<some password>';
ALTER USER taskcafe WITH superuser;
EXIT;
```

On peut maintenant configurer les variables et migrer la base de donnée.

```bash
TASKCAFE_DATABASE_PASSWORD="<some password>" ./dist/taskcafe migrate
```

## Configuration du DNS et HTTPS
Tout d'abord on va créer un nouveau sous-domaine

| Domaine | TTL | Type | Target |
| ------- | --- | ---- | ------ | 
| `tasks.domain.tld` | `0` | `A` | `<ipv4>` |
| `tasks.domain.tld` | `0` | `AAAA` | `<ipv6>` |

Maintenant on peut certifier le domaine avec HTTPS

```bash
apt install -y letsencrypt
systemctl stop nginx
certbot certonly --standalone -d tasks.domain.tld
systemctl start nginx
```

## Configuration nginx
On peut maintenant ouvrir un nouveau fichier de configuration pour le nom de domaine.

```bash
vim /etc/nginx/sites-available/taskcafe.conf
```

Et copier-coller la configuration suivante

```nginx
server {
	listen 80;
	listen [::]:80;

	server_name tasks.domain.tld;

	return 301 https://tasks.domain.tld$request_uri;
}

server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	include /etc/nginx/snippets/letsencrypt.conf;

	server_name tasks.domain.tld;

	
	location / {
            proxy_pass http://127.0.0.1:3333;
	    proxy_set_header Host $host;
	    proxy_set_header Connection       $http_connection;
	    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	    proxy_set_header X-Scheme $scheme;
	    proxy_set_header X-Script-Name /;
	    proxy_buffering off;
	}	

	ssl_certificate /etc/letsencrypt/live/tasks.domain.tld/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/tasks.domain.tld/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/tasks.domain.tld/fullchain.pem;

	access_log /var/log/nginx/tasks.domain.tld.access.log;
	error_log /var/log/nginx/tasks.domain.tld.error.log;
}
```

Enfin on va ajouter la configuration dans les sites activés.

```bash
ln -s /etc/nginx/sites-available/taskcafe.conf /etc/nginx/sites-enabled/
```

## Lancer le tout proprement
On peut maintenant créer un nouveau script dans le dossier `taskcafe`

```bash
vim start
```

Et voici le code du script

```bash
#!/bin/bash
TASKCAFE_DATABASE_PASSWORD="<password>" /path/to/taskcafe/dist/taskcafe web
```

Ensuite il faut juste le définir comme exécutable

```bash
chmod +x start
```

On va maintenant créer un nouveau service systemd.

```bash
vim /etc/systemd/system/taskcafe.service
```

Et coller la configuration suivante :

```systemd
[Unit]
Description=Taskcafe
After=network.target

[Service]
Restart=always
RestartSec=1
ExecStart=/path/to/taskcafe/start

[Install]
WantedBy=multi-user.target
```

Enfin, il suffit de lancer le service

```bash
systemctl daemon-reload
systemctl restart taskcafe
systemctl status taskcafe
```

Le site devrait être disponible à l'addresse : `tasks.domain.tld`

Merci de m'avoir lu 😀
