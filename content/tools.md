+++
title = "The tools I use on my computer"
date = 2021-08-08T14:43:36+02:00
+++
I've already made a similar post in the past, this one is an updated version of it. Note that all the software presented here are free software (or at least open source), except those marked in *italic*.<!-- more -->

## 🍙 Rice
> **Note**: All my ricing is available on my [git repo](https://codeberg.org/SnowCode/rice)

* [dwm](https://dwm.suckless.org): a very minimal tiling window manager. I don't want to spend hours configuring, so I took the most basic, light and minimal one I could find, dwm is an excellent candidate. 
* [dmenu](https://tools.suckless.org/dmenu): the default application menu for dwm.
* [alacritty](https://github.com/alacritty/alacritty): a lightweight very customizable terminal emulator
* [picom](https://github.com/yshui/picom): a lightweight compositor for X11, I use it to make my terminal more transparent. 
* [feh](https://feh.finalrewind.org): used to set a wallpaper. Wallpapers are nice.
* [brightnessctl](https://github.com/Hummer12007/brightnessctl): integrated into dwm's shortcuts to manage brightness
* [pactl](https://www.freedesktop.org/wiki/Software/PulseAudio/): also integrated into dwm's shortcuts, but to manage volume this time.

## ✉️ Communication
* [aerc](https://git.sr.ht/~sircmpwn/aerc): my favorite mail client, I also made [a post about it](https://snowcode.ovh/posts/aerc).
* *[Discord](https://discord.com)*: Unfortunately not open source, very bloated and not privacy-respecting. But it's the best chatting app I know and cannot find any equivalent (closed source or open source).
* [Signal](https://signal.org): A pretty good alternative to Whatsapp.
* [Pleroma](https://pleroma.social): A lightweight server to go on the fediverse (Mastodon, Pixelfed, Peertube, etc). I also made [a post about it](/pleroma).
* [Husky](https://git.mentality.rip/FWGS/Husky): An Android client of Pleroma.
* [Weechat](https://weechat.org)+[tmux](https://github.com/tmux/tmux)+[ssh](https://www.openssh.com): This combo of 3 softwares is the way I use IRC, weechat is the client inside a tmux session (so it remains always active, retrieiving messages) which I can access through ssh. That way, all my messages are synchronized whatever the device. I also made [a post about it](/irc).

## 🌐 Internet and medias
* [Firefox](https://mozilla.org/firefox): A privacy respecting, secure web browser. Which can also be customized a lot.
* *[YouTube](https://www.youtube.com/watch?v=dQw4w9WgXcQ)*: I don't like YouTube, but there's just too many great content there to miss it.
* [NewPipe](https://newpipe.net): Best YouTube, Peertube, Soundbloud, MediaCCC (I don't even know what it is) and Bandcamp client. No ads, listen in the background, view as popup or download any video or music you like! I also use *NewPipe Polish* (I couldn't find its source code though) because when NewPipe is broken, *Polish* usually works.
* *[Reddit](https://old.reddit.com)*: A website that have the ability to consume your time. But there's r/unixporn, so it's good.
* [Slide](https://github.com/ccrama/Slide): An app that help you loosing your time on your phone as well!

## 🔥 CLI tools
* [nano](https://www.nano-editor.org/): Objectively the best text editor in the world, nano rocks.
* [nmtui](https://wiki.gnome.org/Projects/NetworkManager): To manage WiFi connection in your terminal quickly and easily.
* [procps-ng](https://gitlab.com/procps-ng/procps): Monitoring tools for linux. I mostly use "top" but there's also "free", "ps", "uptime", "atop", "slabtop", "vmstat" and "w". I have to admit I don't know what the last 4 do.
* [git](https://git-scm.com): Probably one of the most useful CLI tools there is.
* [ffmpeg](https://ffmpeg.org): A tool to record audio and audio, make basic video editing, compress videos, and do pretty much anything.
* [youtube-dl](https://youtube-dl.org): Download videos and audio from many different websites, including YouTube, Soundcloud, etc.
* grep, sed and awk: Very cool for bash scripting and all. Super fun and useful.

## 🖥  GUI tools
* [mupdf](https://mupdf.com): Read PDFs. Very lightweight, minimal interface: just the content, controlled using keyboard shortcuts.
* [sxiv](https://github.com/muennich/sxiv): Watch images, minimal interface, just the images, controlled using keyboard shortcuts.
* [mpv](https://mpv.io/): Watch videos (when youtube-dl is installed, it can also stream videos from online sources). Also controlled using keyboard shortcuts with a minimal interface.
* [Shotcut](https://www.shotcut.org): Edit videos. Pretty minimal tool but it does the job well.
* [Krita](https://krita.org): Edit images. Very easy to use.

## 💻 Self-hosting and websites
* [nano](https://www.nano-editor.org/): Objectively the best text editor in the world, nano rocks.
* [hugo](https://gohugo.io): A tool to make static websites easily. I also made [a post about it](/hugo) btw. But it may be out of date.
* [tailwind](https://tailwindcss.com): A tool that makes CSS super easy! It's also one of the best documentation I've ever seen.
* [Pleroma](https://pleroma.social): A lightweight server to go on the fediverse (Mastodon, Pixelfed, Peertube, etc)
* [Weechat](https://weechat.org)+[tmux](https://github.com/tmux/tmux)+[ssh](https://www.openssh.com): This combo of 3 softwares is the way I use IRC, weechat is the client inside a tmux session (so it remains always active, retrieiving messages) which I can access through ssh. That way, all my messages are synchronized whatever the device.

## 💿 OS
* [Arch Linux](https://archlinux.org): Very fun to install and use, also one of the best documentation about... everything related to linux.
* [Debian](https://debian.org): My other computer doesn't support Arch, so I use Debian. Very minimal and very nice as well. I also use it for my server. I also made a post about [Debian servers](/secure)

Notable mentions:

* [Xubuntu](https://xubuntu.org): A pretty cool operating system based on Ubuntu. It's also pretty light. (XFCE)
* [Kubuntu](https://kubuntu.org): A pretty cool operating system based on Ubuntu. It is not light though. (KDE)
* [Ubuntu](https://ubuntu.org): A pretty cool operating system. It's not light at all though (GNOME)
* [ElementaryOS](https://elementary.io): Yet another operating system based on Ubuntu. Not light but very nice design. (Pantheon)
