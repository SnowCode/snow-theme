+++
title = "My softwares recommendations"
date = 2021-02-04T13:32:30+01:00
+++
Hello! In this post I am going to show you great softwares (at least according to my standards). My standards for a good software are:<!-- more -->

* A light software, it should not eat all your memory and CPU
* An open-source software ('libre'), this is a guartee the software should be trust worthy, you should get support by the community and it lets you change the code itself if you need to.
* A privacy respecting software, even though the software should be light, it should also protect the privacy and security of their users.
* If online, we should be able to self-host it.
* Minimal, the UI of the app should not be complex. A software should be a tool, therefore, you should be able to understand your tool before using it.

## The base system
Let's start with the basics, what are the system I use. I don't use macOS or Windows because those OSes doesn't respect any of my standards. I use
Linux, I started by using Ubuntu (which is the most popular Linux *distribution*). But now I am using `Arch` and `Debian` and I am thinking about 
installing `Gentoo`. 

Some quick notes for beginners, Ubuntu isn't that all bad unlike some people on 4Chan and Reddit says :p I don't recommend using the classic edition,
especially if you don't have a good hardware. But I like some derivatives such as `Xubuntu`, `Linux Mint`, `Lubuntu`, etc. That have a much more 'classic' experience.

I am not using those anymore now. I am using an interface called `dwm`. **dwm** stands for *dynamic window manager*. Unlike what most people are using,
there is no 'desktop', Windows aren't made to be moved around and the whole thing is designed to be light, customizable and keyboard-driven only.

The experience of using *tiling winow managers* is very different from what you probably know, but once you know the few shortcuts, you become a god on your keyboard :p

I am thinking about creating a configuration guide for dwm soon, just to warn you, dwm isn't 'hard', but it's different and require an adaptation time.

## For the web
For web browsing I use `Firefox`. I used Brave (based on Chromium) in the past, but it didn't respect my standard of lightness and minimalism. Firefox is a good software
especially when you tweak it to improve your privacy, security and remove useless stuff from the top bar. 

But I also use a bunch of add-ons that I find very useful, such as `uBlock Origin`, an ad-blocker which is better than the classic "AdBlock" and "AdBlock+". 
I also use an obscure add-on called `Vimium`. This extension makes you able to browse almost all websites using your keyboard, so you don't have to break your 'keyboard' workflow :p.

## Programming and system setup
I am a very bad programmer, but I love creating tin yscript to automate things, make statistics or calculations. I really LOVE the terminal. I use tons
of commands for many things that people are mostly using GUI stuff for.

* I write my code using `vim` or `nano`
* I write text (including this post) using `vim` or `nano` (I am using nano at the moment) in `markdown` (it's a kind of simplified HTML syntax).
* I encrypt files and messages using `zip` (for directory) or `gpg` (for individual files or text)
* I setup my network and wifi using `nmtui`
* I setup different screens using `xrandr`
* I monitor my CPU and RAM usage with `top`
* I monitor my storage space using `df`
* I check my system's informations using `neofetch`

## Online services
This is where I get some 'weak' points in my standards, because this is where the things become social, and not only individual. Therefore you need other people to follow
you in those services.

I use `mastodon` instead of Twitter or Facebook. I don't use Instagram or Snapchat (which I honestly don't see the point) but I know an alternative to
Instagram is `pixelfed`. 

I also sometimes use `irc` or `xmpp` instead of things like Discord, but I haven't found communities and people as good as the ones I have on Discord yet.

For Google, I use searx. Searx is a 'meta-search engine' you can install locally on your computer. A 'meta-seach engine' is a search engine, that instead of having
it's own database, it using other websites like Google, Wikipedia, Bing, Yahoo, DuckDuckGo, etc. What searx does is basically anonymizing your data before sending them to other 'real' search engines.

For Google Drive, DropBox, WeTransfer or MediaFire. I self-hosted NextCloud that can let me share and store some of my files.

I still use Gmail though, but I may change server to something else in the future. But I don't use the online web interface that Gmail, I use `Thunderbird` instead and I would like to setup `mutt` one day (to manage mails in terminal). 

I also use other services that aren't in this list, like a self-hosted `BookStack`. It's a kind of wiki, but sorted by shelves, books, chapters, pages and sections. Which makes it way more handy than using a Wiki.

For an alternative to Reddit, I use Lemmy, even though there is a part of the Lemmmy community I don't really like, there's still plenty of amazing people there.

## Mobile
Another weak point is about my phone usage. I still use Android after all that time, but Android is difficult to replace. Alternatives exist, like FairPhones that uses a modified version of Android called `lineageOS` that is purged from all Google apps. But FairPhones are expensives
and `lineageOS` is not available on all devices.

But still, I use a $h1t ton of free and open-source apps. This is the list:

* `KISS Launcher` this is an alternative launcher application that is made to save you battery life, repsect your privacy, be light and minimal.
* `FDroid` is an alternative PlayStore, from which you can install most apps that are listed here. All the apps are open-source.
* `Simple Apps` such as voice recorder, contacts, dialer, notes, etc. It's all in the name 'simple'.
* `VLC` good app to watch videos
* `Vinyl` good audio player.
* `Slide`, a Reddit client.
* `Signal`, a SMS app and alternative to WhatsApp
* `OsmAnd~`, an alternative to `Maps`
* `OpenNoteScanner`, everything is in the name :p
* `Open Camera`, everything is in the name :p
* `NewPipe`, best YouTube, Soundcloud, Peertube and MediaCCC client ever made. No ads, listen in background, popup, download, playlists, etc.
* `Firefox Klar`, best web browser for Android imo. 
* `Camera Roll`, alternative to the default gallery.
* `Calculator++`, a nice calculator app that also supports more advanced stuff.
* `AnySoftKeyboard`, an alternative keyboard.
* `Aegis`, an app for 2OT.





