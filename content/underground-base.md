+++
title = "How to make an underground starter base"
date = 2021-07-03
[taxonomies]
tags = ["tutorial", "minecraft"]
[extra]
image = "/underground-base/2021-06-27_13.11.08.png"
+++

I've done TONS of screenshot while playing minecraft to remind myself of the steps to build stuff in survival. So I decided I would publish them as tutorials here. Here is the first one with an underground starter base.<!-- more -->

## Step 1: Gather all the building blocks
Here is the list of all the blocks you will need to build this base.

* 35 spruce slabs
* 31 spruce logs
* 33 spruce planks
* 9 glass blocks
* 3 spruce trapdoors
* 7 torches
* 1 bed
* 9 barrels
* 1 crafting table
* 2 furnaces
* 3 ladders
* 9 spruce stairs
* 4 stone pressure plates

Which means you will have to collect a little more than a stack of wood logs for this (about 70 logs). I personally use giant spruce trees for that, you can easily get a full stack of wood with one of them.

> Note: If you don't have spruce, it's not a big deal, it works with any type of wood.

## Step 2: Build it!
Here are the screenshots of all the steps to build this underground base.

Build the top
![](/underground-base/2021-06-27_13.05.37.png)
Dig a 3x3x4 hole
![](/underground-base/2021-06-27_13.05.59.png)
Dig all the dirt and stone under the slabs and logs
![](/underground-base/2021-06-27_13.06.32.png)
Build the inside skeleton with logs
![](/underground-base/2021-06-27_13.07.10.png)
Dig the "arcs"
![](/underground-base/2021-06-27_13.07.35.png)
On the "ladder side", fill with planks. Otherwise, dig a 3 block hole on the middle
![](/underground-base/2021-06-27_13.07.50.png)
Fill the 2 sides with planks
![](/underground-base/2021-06-27_13.08.16.png)
Place 3 stairs on the top and 3 barrels in the middle
![](/underground-base/2021-06-27_13.08.40.png)
On one side, place a bed, on another place 2 furnaces and 1 crafting table.
![](/underground-base/2021-06-27_13.09.41.png)
Fill the floor with planks
![](/underground-base/2021-06-27_13.10.00.png)
Place torches as such
![](/underground-base/2021-06-27_13.10.13.png)
Cover the middle torch with carpets
![](/underground-base/2021-06-27_13.10.22.png)
Place some pressure plates and glass on the top area.
![](/underground-base/2021-06-27_13.11.08.png)
