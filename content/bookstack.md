+++
title = "Install Bookstack on nginx"
date = 2021-03-12T21:46:47+01:00
+++
Bookstack is a nice alternative to traditional wiki softwares. Here's a little guide to install it.<!-- more -->

## Step 1: Install dependencies
Run the following command to install all the latest php versions:

```bash
sudo apt install -y php php-fpm php-mysql php-curl php-mbstring php-ldap php-tidy php-xml php-zip php-gd composer
sudo apt install -y default-mysql-server default-mysql-client
sudo apt install -y sendmail
```

## Step 2: Download the source code
Just run the following command to download the latest release of Bookstack.

```bash
git clone https://github.com/BookStackApp/BookStack.git --branch release --single-branch
cd BookStack
```

## Step 3: Install from the source code
Just use `composer` and `php` to build and install from the source code.

```bash
composer install --no-dev
```

## Step 4: Configure your database
Then we'll have to create our database for BookStack

```bash
mysql -u root
CREATE DATABASE bookstack_db;
CREATE USER 'bookstackuser'@'localhost' IDENTIFIED BY '<your password here>';
GRANT ALL PRIVILEGES ON bookstack_db.* TO 'bookstackuser'@'localhost';
FLUSH ALL PRIVILEGES;
EXIT;
```

## Step 5: Configure the .env config file
First you need to rename the template `.env.example` into `.env`:

```bash
mv .env.example .env
```

Then you can change the database password in the file (and other variables according to your needs).

## Step 6: Permissions
Now you need to make sure some directories are writable by the web server:

```bash
chown www-data:www-data -R /var/www/BookStack
```

## Step 7: generate the application key and migrate the database
Very easy, simply run:

```bash
php artisan key:generate
php artisan migrate
```

## Step 8: Make the URL rewrite rules in nginx
Now let's create a new configuration file in nginx:

```bash
nano /etc/nginx/sites-available/bookstack.conf
```

Then paste the following content (and replace `books.example.com` by your actual domain):

```nginx
server {
    listen 80;
    listen [::]:80;

    server_name books.example.com;

    return 301 https://books.example.com$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    root /var/www/BookStack/public;

    include /etc/nginx/snippets/letsencrypt.conf;

    index index.php index.html index.htm;

    client_max_body_size 100M;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    server_name books.example.com;

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.4-fpm.sock;
        fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

    ssl_certificate /etc/letsencrypt/live/books.example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/books.example.com/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/books.example.com/fullchain.pem;

    access_log /var/log/nginx/books.example.com.access.log;
    error_log /var/log/nginx/books.example.com.error.log;
}
```

Then, enable the configuration and restart nginx:

```bash
ln -s /etc/nginx/sites-available/bookstack.conf /etc/nginx/sites-enabled/
systemctl restart nginx
```

## Step 9: Connect and change your password
Your instance of BookStack is now available on your subdomain, you can now connect using root account with the following credentials:

| Variable | Value |
| -------- | ----- | 
| Email    | `admin@admin.com` |
| Password | `password` |

Of course don't forget to change them!

## Conclusion
You should now have a full featured BookStack instance up and running.

