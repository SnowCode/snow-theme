+++
title = "The unique and its property"
date = 2022-04-08
[taxonomies]
tags = ["notes", "anarchy", "egoism"]
+++
Currently reading "The unique and its property" from Max Stirner ([link here](https://theanarchistlibrary.org/library/max-stirner-the-unique-and-its-property)) and taking notes in here. The first 4 chapters are available in audio format by *Desert Outpost* [here](https://www.youtube.com/playlist?list=PLHqlKvOF0lYb5TrkBwwBJ2UqbO_uyXsBw)


## Introduction and notes about translation
That book is a new translation made by Wolfi Landstreicher and the translator explains some changes and the general vibe of the text.

He explains that the precedent translation of the text "The ego and its own" was too rigid and made Stirner look like a humorless person. While it's really not the case there are a ton of play on words, sarcasm and satire in all the text. When Stirner says "I based my whole affair on nothing" he basically means "I am having fun".

He also explained the old translation was made by a christian, which according to him is very biaised while reflecting Stirner's views as Stirner was very critical of religion.

Several parts of the texts aren't reflecting Stirner's views but are jokes to prove his point. 

He explains some words used in his translation:

* "Fantasm" means "spook" in the original text, something imaginary (but less haunted than "spook")
* The "unique" is used to talk you and me as individuals, it's something that can't be described (like the dao) as no comparison can be made and is different for every individual
* "Property" is used in the broad sense, not the economical sense. It's used to describe "the whole of your world as you can grasp it", all traits that make an individual.
* "egoism" is not a "believer in the ego" but the idea of "acting in the center of your world".

The translator also explains that he did all of this because he likes plays on words and also as a gift to all the rebels who want to rebel against authority, the sacred and laugh at higher value.

While reading that I also thought a certain concept was really similar to the philosophy of "the absurd" by Albert Camus

> In the face of this overall absurdity, you could choose to ignore it and assume the universality of your own meanings, thus becoming what Stirner called a “duped egoist”; this is the path typical of the religious (including ideologues like Marx and his followers, Hitler and his, or Mises{17} and his). You could let it overwhelm you and fall into a new religion of cosmic pessimism, where the absurdity is a horrifying god (whether you call it by that name or not), and so again become a “duped egoist.” Or you could do what Stirner did and see the humor in the ultimate absurdity, recognizing that this lack of universal meaning and purpose is what gives you and I the capacity to willfully create our lives for ourselves.

This is literally what Camus talk about when talking about suicide, philosophical suicide and rebelling. It seems Camus stole Stirner :P

## I Have Based My Affair on Nothing

