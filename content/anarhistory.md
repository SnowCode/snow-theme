+++
title = "Why I'm an anarchist"
date = 2022-04-28 23:00:39+01:00
[taxonomies]
tags = ["anarchy" ]
+++

I started playing Minecraft in primary school around 2014, I think I started playing when the 1.8 release just came out.

I was and still is a shit at survival mode of the game what I really liked however was making structures, redstone and command blocks in creative mode.

So redstone and command blocks were probably my first use of programming. Then I tried making Minecraft servers as well. I didn't understood shit and was blindly following tutorials, I just couldn't understand the point of port forwardding and used ridiculously complex and unstable methods to circumvent the need to learn to use the web interface of my modem.

I learned stuff like Batch scripting (on Windows, absolutely horrible btw). And I don't exactly know how or why, I once installed Ubuntu in VirtualBox and really enjoyed playing with it. To the point I actually installed it on my computer.

From there I discovered Linux (I think the first distro I installed was Ubuntu or Kali) and open source, I don't exactly know how though. 

From open source I discovered free software and "hacker stuff". Which was my first encounter with anarchist practice.

## Communism
I don't exactly remember how, but I started to get into communism and theorical politics. Especially postleft, with the cringiest YouTube channels in existence such as "NonCompete" that literally advocated for anarcho-prisons. Which is totally not compatible with anarchy what so ever.

I joined Lemmy.ml website. I was in a very moralist notion of things. Left == good, so communism and anarchism == good stupid type of thinking. During the same time, I joined an anarchist Discord server in which I talked about my very utopian and not realistic views of anarchy.

## Postleft
I was taking anarchy as a religious Cause, spreading "the good word" of my ideology and probably also pissing off everybody around me. Then someone said "wait til you learn about postleft". I was curious so I asked them about it, and they sent me a very very boring definition:

> Post-left anarchy is a recent current in anarchist thought that promotes a critique of anarchism's relationship to traditional left-wing politics, such as its emphasis on class struggle, social revolution, labor unions, and the working class. Influenced by anti-authoritarian postmodern philosophy, post-leftists reject Enlightenment rationalism and deconstruct topics such as gender

Which didn't help me much, then the [Raddle's wiki](https://raddle.me/w/postleft) really helped me understand it by more simple terms. I was also kinda shocked by the most tankie side of Lemmy that the admins are. Like with the sister community of Lemmy which is "lemmygrad" on which the first post I found was a "happy birthday stalin" post. 

The idea that they worship a literal dictator seemed so against my views. I started to realize the myth that is "left-unity". 

During the same time I was really wondering about what *I* could do. My first encounters with postleft texts was "Bob Black - Abolition of Work" and "The curious george brigade - Anarchy in the Age of Dinosaurs". 

I was sick of some pretty stupid trends in those political circles. Trends like trying to discuss a "blueprint" for a perfect society, or arguing endlessly on stupid theorical details made by dead men in the precedent century. 

Or people doing action who also despite those who don't follow their actions and say that others are "not doing it right" or "are too individualist". Which I think doesn't help anyone, I don't think we have a duty to spread those ideas, nor do I think it's universally "morally right" to do so.

So this "other" kind of anarchism that puts more emphasis on joy and personal meaning was pretty refreshing to me. It placed action, and autonomy on the first place. It's an idea that was rooted in practice more than in theory and arguments. It made me come back to the ideas that I left when becoming "political", things such as free software for instance.

Looking back, it's funny when I look at those old anarcho-communist theorical books on my bookshelf. They seem so silly and pointless at this point. Practice is so much more interesting than theory to me.

It kinda reminds me of u/Kinshavo on Raddle who said "A prostitute taught me more about anarchy than a drunk Anarchist". 

## Nihilism
Then I also encountered nihilism, egoism, exitentialism and absurdism as well as anticiv thoughts as they are pretty common and associated with postleft.

It's a continous process of spotting and destroying "spooks" (social constructions built to restrict you, such as submission to authority, morality, norms, etc) and also creating personal alternatives and actions.

What those ideas made me realize is that there is no point in trying to achieve "absolute objectivity", "the right moral thing to do", etc. As morality itself a social construction.

It made me realize I am the best to decide what my life will be. Without all the constraints of societal norms.

It also goes with the idea you can create your own personal "meaning" in life, your personal ethics, and norms. 

This also come with a drawback, you can get pretty isolated pretty easily because of this. By being anarchist you already are excluded from all usual statist political discussion. And by being nihilist or postleft, you mostly are also excluded from many political discussion among anarchists themselves, who sometimes seems to prefer theory and blueprints rather than any practical action, or who despise the actions you do by deeming them "lifestylists" for instance.

<!-- Some might say that "free will" also doesn't exist. Which on a global scale is true, my actions are defined by an infinity of factors around me. But from my individual perspective, my perspective defines my vision of reality, so while some might turn to God as a supreme being, I turn to myself, my Ego is my God.  -->

## What I want to talk about
I would like to make more blog posts about all of this because, for me anarchy is much more than a theorical ideology. It's a culture, philosophy, lifestyles, people, actions, stories, etc. It's a very broad spectrum of things.

So I would especially want to talk about some critiques of gender, technology, civilization, etc. But most importantly: actions. I think actions speaks more than a thousands words and they can help you, I and other people *now*. Not in a theorical utopian future.

Some of those actions are: free software, open hardware, hackerspaces, low-techs, self-care (the icarus project, etc), etc. 

Those aren't "theorical solutions" for problems in the world. They are tools we can use as individuals and groups to solve real problems we encounter and take control of our lives. Those stuff are exciting as fuck.
