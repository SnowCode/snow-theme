+++
title = "How to build an efficient mob farm"
date = 2021-07-03
[taxonomies]
tags = ["tutorial", "minecraft"]
[extra]
image = "/mob-farm/2021-07-03_21.16.41.png"
+++
Here is a simple design of a mob farm that doesn't require much redstone but remains very very effective.<!-- more -->

## Step 1: Gather the building blocks
* 1637 spawnable blocks like stone (about 25.5 stacks)
* 840 slabs (about 13 stacks)
* 34 torches
* 9 hoppers
* 9 soul campfire
* 6 chests
* 10 observer
* 10 dispensers
* 26 redstone repeaters
* 3 redstone
* 1 lever
* 1 comparator
* 10 water buckets

> Note: If this is too much blocks for you, you can just build half of it. It will be less efficient, but will still have amazing results.

## Step 2: Building it
> Advice: I really advise you to put torches on each layers, this will avoid mobs from spawning when you're building the thing.

Place 9 hoppers facing 3 double chests

![](/mob-farm/2021-06-27_13.22.46.png)

Make a "slab ring" around the hoppers

![](/mob-farm/2021-06-27_13.23.07.png)

Make a 17x17 block platform on top. Then add a border, torches and water as shown on the picture

![](/mob-farm/2021-06-27_13.35.34.png)

Place one dispenser facing upward, 4 blocks higher than the hoppers

![](/mob-farm/2021-06-27_13.36.33.png)

Make 4 lines of 7 blocks from all the sides of the dispenser

![](/mob-farm/2021-06-27_13.36.52.png) 

Place one dispenser facing upward, 4 blocks higher than the hoppers

![](/mob-farm/2021-06-27_13.37.44.png) 

Place one observer (watching top block) and one dispenser on top

![](/mob-farm/2021-06-27_13.38.04.png) 

Make 9 other layers of this (you can do less if you don't have enough blocks

![](/mob-farm/2021-06-27_13.50.40.png) 

Make a 29x29 slab platform around it.

![](/mob-farm/2021-06-27_13.52.47.png) 

Make some doubleslabs around the dispenser as shown

![](/mob-farm/2021-06-27_13.53.12.png) 

Place all the 26 repeaters on 4 ticks. Also place one lever and one ACTIVATED comparator next to it.

![](/mob-farm/2021-07-03_21.08.22.png) 

Fill all the dispensers with water

![](/mob-farm/2021-06-27_13.54.47.png) 

Create an AFK platform 92 blocks above the top of the farm. When you want to activate the farm, simply pull the lever.

![](/mob-farm/2021-07-03_21.16.41.png) 
