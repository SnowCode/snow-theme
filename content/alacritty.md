+++
title = "My Alacritty Setup"
date = 2021-04-29T18:25:43+02:00
+++
## Why I use Alacritty
I am obsessed with lightness and optimization, I try ot reduce CPU and RAM usage everywhere, and try to keep the interface minimalistic.<!-- more -->

I first used `tilix`, but I didn't like it because it's not minimal and optimized enough for me.

Then I used `st`, it's maybe one of the most minimal and light terminal emulator out there (written in pure C), but It wasn't stable enough (mine was crashing when using commands with a lot of output) also, you need to recomile the terimnal at every modification.

Meanwhile, `alacritty` is written in Rust (which is very fast), but unlike `st` the modifications are live, which means you simply have to edit a file and the changes are applied automatically.

## Color scheme manager
The "classic" way to edit your configuration in Alacritty is to modify your `~/.config/alacritty/alacritty.yml` or `~/.alacritty.yml` file by copy-pasting code from other configurations.

But you can also use the package `alacritty-themes` to choose in a large directory of themes simply by pressing "ENTER". 

`alacritty-themes` is written in Javascript, so you need to have `node` and `npm` installed. This is how to install it:

```bash
npm install -g alacritty-themes
ln -s /usr/local/node/bin/* /usr/bin/
```

Then you simply have to run `alacritty-themes` and pick your favorite :P My current favorite is `dracula`.

## Fonts
For fonts, I first used `Share Tech Mono`, then I switched to `Fira Code` which is really good. It's the one I am using now.

I had no idea of what to choose as terminal font, so I watched [this video](https://www.youtube.com/watch?v=xozmOVOWPtg).

## Conclusion
There is not a lot of things to say about a terminal setup in my opinion, but If you really want to see my config: [this](https://codeberg.org/SnowCode/gist/src/branch/main/alacritty.yml) is my current configuration.



