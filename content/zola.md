+++
title = "How to make a Zola theme easily"
date = 2021-12-18 18:22:39+01:00
[taxonomies]
tags = ["tutorial", "announcement"]
[extra]
video = "/zola.mp4"
+++
I just re-designed my blog and switched to Zola today! I plan on adding more content soon (Minecraft tutorials, other tutorails, maybe some essays + some videos). But for now this is just a classic tutorial about making a Zola template.<!-- more -->

## Why Zola
Zola is a static website generator written in Rust. It's a very nice alternative to Hugo which I used before, though the two are very similar I prefer the way Zola handles templates.

For comparison, this was my hugo site:

```
.
├── archetypes
│   └── default.md
├── config.toml
├── content
├── data
├── layouts
├── public
├── static
└── themes
    └── darkmin
        ├── archetypes
        │   └── default.md
        ├── layouts
        │   ├── 404.html
        │   ├── _default
        │   │   ├── baseof.html
        │   │   ├── list.html
        │   │   ├── rss.xml
        │   │   ├── single.html
        │   ├── index.html
        │   └── partials
        │       ├── footer.html
        │       ├── header.html
        │       ├── head.html
        │       ├── post.html
        │       └── posts.html
        ├── static
        ├── LICENSE
        └── theme.toml

13 directories, 16 files
```

And this is my Zola website: (taxonomy_list is currently empty)

```
.
├── config.toml
├── content
│   ├── _index.md
│   └── test.md
├── public
├── sass
├── static
├── templates
│   ├── base.html
│   ├── index.html
│   ├── page.html
│   ├── taxonomy_list.html
│   └── taxonomy_single.html
└── themes

6 directories, 8 files
```

Also, note that my precedent hugo theme didn't support taxonomy (tags and categories, while this one does).

Writing a tutorial for something that varries to much is always pretty hard so I invite you to check out [the repo](https://codeberg.org/SnowCode/snow-theme). Here I'll explain each file and 

## Step 1: Install Zola
Now let's install Zola. I don't feel like giving exact instructions for this because it depends on your operating system. So I let you checkout the [instructions](https://www.getzola.org/documentation/getting-started/installation/) by yourself.

Once zola is installed you can run the following commands to create a new zola project and host it locally:

```bash
zola init <project name>
cd <project name>
zola serve
```

## Step 2: Do stuff
Once zola was installed and running what I did was pretty much this:

1. Making a wireframe, picking some fonts, choosing a color scheme.
2. Taking templates from other templates and changing them to fit my usage. I used the [Zola documentation](https://www.getzola.org/documentation/templates/overview/) for this.
3. Adding tailwind + tailwind classes (for mobile then for computer to be reponsive). I used the [tailwind documentation](https://tailwindcss.com) for this.
4. Testing and adding content

To be honest my process wasn't as smooth as this lol but I would say that's what you should do. See the video above to see my full process and struggle. 

## Step 3: All files
Here is the organisation of files in my theme:


```
.
├── config.toml
├── content
│   ├── _index.md
│   └── test.md
├── public
├── sass
├── static
├── templates
│   ├── base.html
│   ├── index.html
│   ├── page.html
│   ├── taxonomy_list.html
│   └── taxonomy_single.html
└── themes

6 directories, 8 files
```

* config.toml defines all the variables of the website (base_name, custom variables, taxonomies (tags, categories, etc), code highlighting, RSS feed, etc)
* content/ is the directory which contains all the blog posts and pages. Each post has a head which looks like this:

```markdown
+++
title = "Your title here"
date = 2021-12-18 19:47:18+01:00
[taxonomies]
tags = ["first", "second"]
[extra]
image = "some link goes here"
+++
```

> Note: extra and taxonomies are optional

* public/ is where the static HTML files will be generated when running "zola build"
* sass/ to put some sass files, I use tailwind so I don't need that
* static/ to put images and all sorts of files there
* templates/ is for the templates (obviously)
    * base.html is for the header and the footer (same for all files)
    * index.html is for the list of all pages
    * page.html is for generating the content of a page/post.
    * taxonomy_list.html is for listing tags (and others). The file is empty at the moment
    * taxonomy_sinle.html is pretty much the same as index.html, it lists all the posts in given categories/tags.
* themes/ ??? magic

## News on this blog
This is more a note to self but I plan on finsihing and publishing some texts and essays that are sleeping on my computer, I also plan on adding my precedent YouTube videos to this blog with a transcript and updates and of course, making more blog posts.

I also want to diversify my type of posts, I made mostly tutorials to this point but I would like to make other type of stuff as well here...
