+++
title = "Descending into Madness"
date = 2022-04-07 18:22:39+01:00
[taxonomies]
tags = ["anarchy", "psychiatry", "notes"]
+++

Those are my notes about the text ["descending into madness" from Flower Bomb](https://theanarchistlibrary.org/library/flower-bomb-descending-into-madness-an-anarchist-nihilist-diary-of-anti-psychiatry).<!-- more -->

## First half
I wrote a good half before starting to take notes so I may add this part later.

## Second half
> What we say is the truth is what everybody accepts. ...I mean, psychiatry: it's the latest religion. We decide what's right and wrong. We decide who's crazy or not. I'm in trouble here. I'm losing my faith

Psychiatry doesn't want to eliminate problems but manage the "symptoms" of "disorders". 

The symptoms are set of behaviors or emotional responses that indicate an individual's struggle to conform to societal norms.

Disorders are behaviors or emotional responses condemned by society and declared "mental illness" by authority of psychiatry.

The sources can be a lot of things, including coercion, civilized society, pressure, etc.


Information can also be demonized, rebelling becomes a mental illness. This was the case with "psikhushka hospitals" in soviet unions, rebelling against soviet authority was considered schizophrenia.

> The Law, social expectation, and psychiatric tradition and practice point to coercion as the profession’s paradigmatic characteristic. Accordingly, I define psychiatry as the theory and practice of coercion, rationalized as the diagnosis of mental illness and justified as medical treatment aimed at protecting the patient from himself and society from the patient

Who enforces definitions and universal truths? Is it the same psychiatric authority that considered homosexuality a mental illness?

Madness are the best toolsfor understanding the effects of society on well being.

> This fear also plays a vital role in creating an obsession with relying on institutional specialization rather than peer to peer support. This obsession is normalized when, in response to someone reaching out for emotional support, friends suggest ‘professional help’ as if to surrender themselves ineffective by default.

Pyschiatry is not useful for understanding human behavior, humans are complex, people are far more than disorders.

Psychiatric labels and identities are tools of the state, a tool for civilization, to create alienation and violence by treating people who could not conform "broken" and therefore inferior.


