+++
title = "Ma config dwm (et installation de ly)"
date = 2021-02-06T15:01:40+01:00
+++
Salut ! Voici un petit guide sur ma twm (Tiling Window Manager) préférée. DWM est un TWM minimaliste et extrêmement light écrit en C. <!-- more -->

Il n'y a pas de "dotfile", vous éditez le code source lui-même :p Donc la configuration est un peu différente des autres TWM, c'est pour cela que je fais un guide.


## Téléchargement
Tout d'abord on va télécharger le code source de `dwm`

```bash
sudo apt install dwm suckless-tools feh
git clone git://git.suckless.org/dwm
cd dwm/
```

La commande APT va servir à installer toutes les dépendances nécessaires pour nous.

## Comprendre ce guide
Dans ce guide je vais utiliser une formulation pour l'édition des fichiers de configuration. Elle dérive de `git diff`

```C
- Ligne à supprimer
+ Nouvelle ligne pour remplacer la précédente
```

## Configuration du clavier et des tags (workspaces)
Tout d'abord on va entrer dans le fichier de configuration:

```bash
nano config.def.h
```

Je n'aime pas beaucoup la touche `ALT` qui est utilisée par défault. Je préfère la touche `WIN` :

```C
- #define MODKEY Mod1Mask
+ #define MODKEY Mod4Mask
```

Je vais ensuite supprimer des 'tags', ces 'tags' sont aussi appellés 'workspaces' dans d'autes TWMs.

```C
- static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
+ static const char *tags[] = { "1", "2", "3" };
```

Je vais aussi supprimer des "règles", ces règles définissent dans quel tag chaque fenêtre va s'ouvrir, je n'ai jamais compris le but de ce truc. 

```C
- { "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
```

J'utilise un clavier AZERTY, donc les raccourcis de claviers pour changer de tags ne sont plus les mêmes. Rajouter à cela que l'on a supprimé 6 tags.

```C
- TAGKEYS(                        XK_1,                      0)
- TAGKEYS(                        XK_2,                      1)
- TAGKEYS(                        XK_3,                      2)
- TAGKEYS(                        XK_4,                      3)
- TAGKEYS(                        XK_5,                      4)
- TAGKEYS(                        XK_6,                      5)
- TAGKEYS(                        XK_7,                      6)
- TAGKEYS(                        XK_8,                      7)
- TAGKEYS(                        XK_9,                      8)

+ TAGKEYS(                        XK_ampersand,              0)
+ TAGKEYS(                        XK_eacute,                 1)
+ TAGKEYS(                        XK_quotedbl,               2)
```

On peut encore changer tous les autres raccourcis de clavier, mais j'aime bien ceux qui sont par défault, donc je vais les laisser.

## Ajouter une bordure, des couleurs et des 'gaps'
On va commencer par le plus simple: la bordure et la couleur par défault.

```C
- static const unsigned int borderpx  = 1;
+ static const unsigned int borderpx  = 2;

- static const char col_cyan[]        = "#005577";
+ static const char col_cyan[]        = "#23eafc";
```

Une fois que l'on a changé notre belle bordure, on va ajouter des gaps. Cela est un peu plus compliqué, donc on va utiliser un patch. 

Voici les commandes à utiliser pour installer le patch:

```bash
wget https://dwm.suckless.org/patches/uselessgap/dwm-uselessgap-20200719-bb2e722.diff
patch -i dwm-uselessgap-20200719-bb2e722.diff
rm dwm-uselessgap-20200719-bb2e722.diff
```

Une fois cela fait, on va changer la largeur de ceux-ci:

```C
- static const unsigned int gappx     = 6;
+ static const unsigned int gappx     = 20;
```

Ensuite on va compiler dwm

```bash
rm config.h
sudo make clean install 
```


## Status bar et fond d'écran
Ensuite, on va créer un script pour lançer dwm

```bash
sudo vim /usr/bin/start_dwm
```

Voici le code qui va être dans ce script : 

```bash
while xsetroot -name "`cat /sys/class/power_supply/BAT1/capacity`% | `date`"
do
	sleep 1
done &
feh --bg-scale <path to the image>
exec dwm
```

Enfin on va rendre le script exécutable et l'ajouter dans `/usr/share/xsessions/` :

```bash
sudo vim /usr/share/xsessions/dwm.desktop
```

Voici le code du fichier :

```bash
[Desktop Entry]
Encoding=UTF-8
Name=dwm
Comment=Dynamic window manager
Exec=start_dwm
Icon=dwm
Type=XSession
```

Maintenant il ne reste plus qu'a se déconnecter et rejoindre dwm :p

```bash
pkill -u $USER
```
## Installer ly à la place de lightdm ou de gdm
Je n'aime pas trop `lightdm` ou `gdm`, donc j'ai choisis d'installer `ly` à la place. Voici comment l'installer:

Tout d'abord on va télécharger le code source

```bash
git clone https://github.com/nullgemm/ly
cd ly/
```

Maintenant on va installer toutes les dépendences:

```bash
sudo apt install build-essential libpam0g-dev libxcb-xkb-dev
```

Ensuite on peut compiler le tout :

```bash
make github
make
sudo make install
```

Puis l'activer

```bash
sudo systemctl disable lightdm gdm
sudo systemctl enable ly
```

On peut ensuite reboot l'ordi.

```bash
reboot
```

Et voilà !

